# omnileads-digitalocean
Deploy your own Contact Center as a Service business on Digitalocean with OMniLeads & Terraform

<p>OMniLeads (OML) is an Open Source software solution based on WebRTC technology(https://webrtc.org/) designed to support the management, operation and administration of a Contact Center using multiple comunication channels. At present it allows the management and phone attention deployment using: Inbound Campaigns, Preview Campaigns and Manual Outbound Campaigns natively. Also it have with the option to administrate Predictive/Progressive Dialer Campaigns using integrations APIs.</p>

<p>In this repository you will find the terraform code necessary to deploy OMniLeads on digitalocean in an automated way and isolating the main components of the App in such a way that the security of business data and the ease of scaling any component prevail.</p>

docs: https://documentacion-omnileads.readthedocs.io/es/latest/install.html#deploy-de-omnileads-basado-en-terraform

## License
GPLv3. Every source code file contains the license preamble and copyright details.
