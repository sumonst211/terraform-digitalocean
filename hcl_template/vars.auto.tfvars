## GENERAL VARS ## GENERAL VARS ## GENERAL VARS
## GENERAL VARS ## GENERAL VARS ## GENERAL VARS
cloud_provider = "digitalocean"
droplet_nic = "eth1"

app = "omlapp"
# Environment tag
environment = "prod"
# CentOS-7 image
img_centos = "centos-7-x64"
# Ubuntu Server image
img_ubuntu = "ubuntu-18-04-x64"
# Docker image
img_docker = "docker-20-04"

callrec_storage = "s3-do"

nfs_host = "NULL"

## SIZING VARS ## SIZING VARS ## SIZING VARS
## SIZING VARS ## SIZING VARS ## SIZING VARS

# OMLapp component droplet size
droplet_oml_size = "s-2vcpu-4gb"
# Asterisk component droplet size
droplet_asterisk_size = "s-1vcpu-1gb"
# RTPengine componenet droplet size
droplet_rtp_size = "s-1vcpu-1gb"
# REDIS component droplet size
droplet_redis_size = "s-1vcpu-1gb"
# Kamailio component droplet size
droplet_kamailio_size = "s-1vcpu-1gb"
# Websocket component droplet size
droplet_websocket_size = "s-1vcpu-1gb"
# PGSQL component digitalocean-cluster size and nodes 
pgsql_size = "db-s-1vcpu-2gb"
cluster_db_nodes = "1"
# Wombat dialer component droplet size
droplet_dialer_size = "s-1vcpu-2gb"
# Wombat dialer DB component droplet size
droplet_mariadb_size = "s-1vcpu-1gb"

## COMPONENETS NAME VARS ## COMPONENETS NAME VARS ## COMPONENETS NAME VARS
## COMPONENETS NAME VARS ## COMPONENETS NAME VARS ## COMPONENETS NAME VARS

# Don't change this variables !!!!
# Don't change this variables !!!!
# Don't change this variables !!!!
name = "customer-name"
oml_tenant_name = "customer-name"
name_rtpengine = "customer-name-rtp"
name_pgsql = "customer-name-pgsql"
name_redis = "customer-name-redis"
name_mariadb = "customer-name-mariadb"
name_wombat = "customer-name-wombat"
name_lb = "customer-name-lb"
name_kamailio = "customer-name-kamailio"
name_websocket = "customer-name-websocket"
name_asterisk = "customer-name-asterisk"
name_haproxy = "customer-name-haproxy"
name_omlapp = "customer-name"
omlapp_hostname = "customer-name"
# OMLapp droplet private NIC
omlapp_nginx_port = "443"

### OMniLeads App vars ### OMniLeads App vars ### OMniLeads App vars
### OMniLeads App vars ### OMniLeads App vars ### OMniLeads App vars

# ********************** Classic deploy
# Braches release to deploy
oml_app_branch="master"
oml_rtpengine_branch="210714.01"
oml_redis_branch="210714.01"
oml_kamailio_branch="210802.01"
oml_acd_branch="210802.01"
oml_ws_branch="210714.01"

# ********************** Docker deploy
oml_app_img="latest"
oml_rtpengine_img="latest"
oml_redis_img="1.0.3"
oml_kamailio_img="latest"
oml_acd_img="latest"
oml_ws_img="latest"
oml_nginx_img="develop"

# ********************* OMniLeads App variables
# Asterisk SIP Trunks allowed ips
ssh_allowed_ip = ["0.0.0.0/0"]
# Asterisk SIP Trunks allowed ips
sip_allowed_ip = ["190.19.150.8/32"]
# Time Zone to apply on Django
oml_tz = "America/Argentina/Cordoba"

# Asterisk AMI USER for OMLApp manager connections
ami_user = "omnileadsami"
# Asterisk AMI PASS for AMI USER OMLApp manager connections
ami_password = "s3cur3_p4ssw0rd"
# Wombat API user to login from OMLapp
dialer_user = "demoadmin"
# Wombat API password to login from OMLapp
dialer_password = "demo"
# PGSQL database name
pg_database = "omnileads"
# PGSQL username for OMLapp
pg_username = "omnileads"
# PGSQL password for OMLapp
pg_password = "s3cur3_p4ssw0rd"
# Session cookie age
sca = "3600"

reset_admin_pass = "true"
init_environment = "false"

# Wombat dialer Component vars
wombat_database = "wombat"
wombat_database_username = "wombat"
wombat_database_password = "s3cur3_p4ssw0rd"

# Backup/Restore params
oml_auto_restore = "false"
oml_app_backup_path = "/opt/omnileads/backup"
oml_app_backup_filename = "NULL"
oml_acd_backup_filename = "NULL"

# Kamailio tweeks 
kamailio_shm_size = "256"
kamailio_pkg_size = "32"

# OMLApp hight load components tweeks
oml_high_load = "NULL"

