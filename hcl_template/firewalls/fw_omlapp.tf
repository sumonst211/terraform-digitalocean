# Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp
  # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp
  resource "digitalocean_firewall" "fw_omlapp" {
    name = var.name_omlapp

    droplet_ids = [module.droplet_omlapp.id[0]]

      # SSH allowed IPs
    dynamic "inbound_rule" {
      iterator = ssh_allowed_ip
      for_each = var.ssh_allowed_ip
      content {
        port_range       = "22"
        protocol         = "tcp"
        source_addresses = var.ssh_allowed_ip
      }
    }
    # HTTPS from LB
    inbound_rule {
      protocol                  = "tcp"
      port_range                = "443"
      source_load_balancer_uids = [module.lb.lb_id]
    }

    outbound_rule {
      protocol              = "tcp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
      protocol              = "udp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
    }
  }
