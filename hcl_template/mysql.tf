#  MARIADB componenet #  MARIADB componenet #  MARIADB componenet #  MARIADB componenet #  MARIADB componenet
#  MARIADB componenet #  MARIADB componenet #  MARIADB componenet #  MARIADB componenet #  MARIADB componenet

  module "droplet_mariadb"  {
   source             = "../../modules/droplet"
   image_name         = var.img_centos
   name               = var.name_mariadb
   tenant             = var.oml_tenant_name
   environment        = var.environment
   region             = var.region
   ssh_keys           = [var.ssh_key_fingerprint]
   vpc_uuid           = module.vpc.id
   droplet_size       = var.droplet_mariadb_size
   monitoring         = false
   private_networking = true
   ipv6               = false
   user_data          = templatefile("../../modules/mysql.tpl", {
     dialer_username            = var.wombat_database_username
     dialer_password            = var.wombat_database_password
     })
   }

   # Firewall aplicado MARIADB # Firewall aplicado MARIADB
   # Firewall aplicado MARIADB # Firewall aplicado MARIADB

   resource "digitalocean_firewall" "fw_mariadb" {
     name = var.name_mariadb

     droplet_ids = [module.droplet_wombat.id[0]]


    # SSH allowed IPs
     dynamic "inbound_rule" {
      iterator = ssh_allowed_ip
      for_each = var.ssh_allowed_ip
      content {
        port_range       = "22"
        protocol         = "tcp"
        source_addresses = var.ssh_allowed_ip
      }
     }

     inbound_rule {
       protocol            = "tcp"
       port_range          = "3306"
       source_droplet_ids  = [module.droplet_wombat.id[0]]
     }

     outbound_rule {
       protocol              = "tcp"
       port_range            = "1-65535"
       destination_addresses = ["0.0.0.0/0", "::/0"]
     }

     outbound_rule {
       protocol              = "udp"
       port_range            = "1-65535"
       destination_addresses = ["0.0.0.0/0", "::/0"]
     }

     outbound_rule {
     protocol              = "icmp"
     destination_addresses = ["0.0.0.0/0", "::/0"]
   }
   }
