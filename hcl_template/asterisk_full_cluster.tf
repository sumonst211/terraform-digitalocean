#  ASTERISK componenet #  ASTERISK componenet #  ASTERISK componenet #  ASTERISK componenet #  ASTERISK componenet
#  ASTERISK componenet #  ASTERISK componenet #  ASTERISK componenet #  ASTERISK componenet #  ASTERISK componenet

module "droplet_asterisk"  {
   source             = "../../modules/droplet"
   image_name         = var.img_centos
   name               = var.name_asterisk
   tenant             = var.oml_tenant_name
   environment        = var.environment
   region             = var.region
   ssh_keys           = [var.ssh_key_fingerprint]
   vpc_uuid           = module.vpc.id
   droplet_size       = var.droplet_asterisk_size
   monitoring         = false
   private_networking = true
   ipv6               = false
   user_data          = templatefile("../../modules/omlutilities/first_boot_installer_omlacd.tpl", {
     oml_acd_release        = var.oml_acd_branch
     oml_tenant_name        = var.oml_tenant_name
     oml_infras_stage       = var.cloud_provider   
     oml_app_host           = format("%s.%s", var.omlapp_hostname, var.domain_name) #var.omlapp_hostname
     oml_redis_host         = module.droplet_redis.ipv4_address_private
     oml_pgsql_port         = module.pgsql.database_port
     oml_pgsql_host         = module.pgsql.database_private_host
     oml_pgsql_db           = var.pg_database
     oml_pgsql_user         = var.pg_username
     oml_pgsql_password     = digitalocean_database_user.omnileads.password
     oml_pgsql_cloud        = "true"
     oml_ami_user           = var.ami_user
     oml_ami_password       = var.ami_password
     oml_callrec_device     = var.callrec_storage
     s3_access_key          = var.spaces_key
     s3_secret_key          = var.spaces_secret_key
     s3url                  = var.spaces_url
     ast_bucket_name        = var.oml_tenant_name
     nfs_host               = "NULL"
     oml_backup_filename    = var.oml_acd_backup_filename
     oml_auto_restore       = var.oml_auto_restore
     oml_tz                 = var.oml_tz
   })
}
