#!/bin/bash

sudo hostnamectl set-hostname ${JITSI_HOSTNAME}
sudo echo "127.0.0.1 ${JITSI_HOSTNAME}" >> /etc/hosts

cd /tmp && wget https://download.jitsi.org/jitsi-key.gpg.key
sudo apt-key add jitsi-key.gpg.key
rm jitsi-key.gpg.key
sudo echo "deb https://download.jitsi.org stable/" >> /etc/apt/sources.list.d/jitsi-stable.list
sudo apt update 
apt -y install prosody jicofo jitsi-meet-web jitsi-meet-prosody jitsi-meet-web-config

sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh





#sudo ufw allow 80/tcp
#sudo ufw allow 443/tcp
#sudo ufw allow 4443/tcp
#sudo ufw allow 10000/udp